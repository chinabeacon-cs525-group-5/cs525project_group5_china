Team Member:
Mingming Lou
Xiaobing Li
Yue Li

Explain:
Go to the path where the extracted file is located.
Run the following command: make
Run. / test_ assign1
To delete an object file, run the following command: makeclean

Function：

initStorageManager（）
initialization program 

createPageFile（）
Check if the file already exists.
If it exists, the user is asked if the file can be overwritten.
If the user can do it, the file opens in write mode.
If the file does not exist, create a new file and add 1 page to it.

openPageFile（）
If the file exists, open it in read mode.
If the file does not exist, an error code is returned.

closePageFile（）
Close the file. If the file does not exist or cannot be opened in read mode, the corresponding error code is returned.

destroyPageFile（）
Delete the existing file and return a success message. If the file does not exist, an error code is returned.

readBlock（）
Move the file descriptor from the beginning of the file to a specific block, read 1 page of data (4096 bytes) and load it into the memory specified by mempage.
Use "ftell" to find the current page location and store it in "curPagePos" for further operation.


getBlockPos（）
Use the file descriptor to find the current page location. If the file handler is not initialized or the file does not open in read mode, an error code is returned.

readFirstBlock（）
Move the file descriptor in the file handler to the beginning of the file and read the first page into "mempage".

readPreviousBlock（）
Move the file descriptor in filehandler to the previous page relative to the current page position, and read the contents of the previous page to the buffer specified by mempage.

readCurrentBlock（）
Set the file descriptor in the file handler to the current page location and read the page content as mempage.

readNextBlock（）
Set the file descriptor in the file handler to the next page position, and read the page content as "mempage".

readLastBlock（）
Set the file descriptor in the file handler as the last page, and read the contents of the page to the buffer specified by "mempage"

writeBlock（）
Check whether the file exists, move the file pointer from the beginning of the file to a specific page, and then write 1 page into it.


writeCurrentBlock（）
Check whether the file exists, move the file pointer from the beginning of the file to the current page, and then write 1 page into it.


appendEmptyBlock（）
Check that if the file exists and can be opened in read mode. If these conditions are met, open the existing file in "read +" mode to write data. The file pointer is set at the end of the file, and a page with the character "\ 0" is added to the newly added empty block. The file handle variable has been updated.

