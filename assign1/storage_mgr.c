#include "storage_mgr.h"
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initStorageManager(void) {
    printf("\n------------Initializing Storage Manager-----------------\n");
    printf("BY\n");
    printf("Xiaobing Li\n");
    printf("Yue Li\n");
    printf("Mingming Lou\n");
    printf("------------Storage Manager Initialized Successfully-------\n");
}

RC createPageFile(char *fileName) {
    FILE *fp = fopen(fileName, "r");
    if (fp != NULL) {
        fclose(fp);
        return RC_FILE_PRESENT;
    }
    fp = fopen(fileName, "w");
    char *memoryBlock = malloc(PAGE_SIZE * sizeof(char));
    memset(memoryBlock, '\0', PAGE_SIZE);
    fwrite(memoryBlock, sizeof(char), PAGE_SIZE, fp);
    free(memoryBlock);
    fclose(fp);
    return RC_OK;
}

RC openPageFile(char *fileName, SM_FileHandle *fHandle) {
    FILE *fp = fopen(fileName, "r+");
    if (fp == NULL)
        return RC_FILE_NOT_FOUND;

    struct stat fileStat;
    stat(fileName, &fileStat);
    int fileSize = fileStat.st_size;
    fHandle->fileName = fileName;
    fHandle->totalNumPages = fileSize / PAGE_SIZE;
    fHandle->curPagePos = 0; // when open new file, we must start with the first page
    fHandle->mgmtInfo = fp;
    rewind(fp); // set the file pointer to the start ot the file
    return RC_OK;

}

RC closePageFile(SM_FileHandle *fHandle) {
    if (fclose(fHandle->mgmtInfo) == 0)
        return RC_OK;
    else
        return RC_FILE_NOT_FOUND;
}

RC destroyPageFile(char *fileName) {
    if (remove(fileName) == 0)
        return RC_OK;
    else
        return RC_FILE_NOT_FOUND;
}

// page number is from 0 to total_page-1
RC readBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    if (fHandle == NULL)
        return RC_FILE_HANDLE_NOT_INIT;

    if (pageNum >= fHandle->totalNumPages || pageNum < 0)
        return RC_READ_NON_EXISTING_PAGE;
    else
        fseek(fHandle->mgmtInfo, pageNum * PAGE_SIZE, SEEK_SET);


    if (fread(memPage, sizeof(char), PAGE_SIZE, fHandle->mgmtInfo) != PAGE_SIZE)
        return RC_FILE_READ_ERROR;

    fHandle->curPagePos = (ftell(fHandle->mgmtInfo) / PAGE_SIZE);
    return RC_OK;
}

int getBlockPos(SM_FileHandle *fHandle) {
    if (fHandle == NULL)
        return RC_FILE_HANDLE_NOT_INIT;
    return fHandle->curPagePos;
}

RC readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(0, fHandle, memPage);
}

RC readPreviousBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(fHandle->curPagePos - 1, fHandle, memPage);
}

RC readCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(fHandle->curPagePos, fHandle, memPage);
}

RC readNextBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(fHandle->curPagePos + 1, fHandle, memPage);
}

RC readLastBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return readBlock(fHandle->totalNumPages - 1, fHandle, memPage);
}


RC writeBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage) {
    if (fHandle == NULL)
        return RC_FILE_HANDLE_NOT_INIT;

    if (pageNum >= fHandle->totalNumPages || pageNum < 0)
        return RC_WRITE_OUT_OF_BOUND_INDEX;

    FILE *fp = fHandle->mgmtInfo;
    fseek(fp, pageNum * PAGE_SIZE, SEEK_SET);

    if (fwrite(memPage, PAGE_SIZE, 1, fp) != 1)
        return RC_WRITE_FAILED;

    fseek(fp, (pageNum + 1) * PAGE_SIZE, SEEK_SET);
    fHandle->mgmtInfo = fp;
    fHandle->curPagePos = (ftell(fHandle->mgmtInfo) / PAGE_SIZE);
    fHandle->totalNumPages = (ftell(fp) / PAGE_SIZE);
    return RC_OK;
}

RC writeCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage) {
    return writeBlock(fHandle->curPagePos, fHandle, memPage);
}

RC appendEmptyBlock(SM_FileHandle *fHandle) {
    if (fHandle == NULL)
        return RC_FILE_HANDLE_NOT_INIT;

    FILE *fp = fHandle->mgmtInfo;

    fseek(fp, 0, SEEK_END);

    char *memoryBlock = malloc(PAGE_SIZE * sizeof(char));
    memset(memoryBlock, '\0', PAGE_SIZE);
    fwrite(memoryBlock, sizeof(char), PAGE_SIZE, fp);
    free(memoryBlock);
    fHandle->totalNumPages++;

    return RC_OK;
}

RC ensureCapacity(int numberOfPages, SM_FileHandle *fHandle) {
    if (fHandle == NULL)
        return RC_FILE_HANDLE_NOT_INIT;

    if (fHandle->totalNumPages < numberOfPages) {
        while (fHandle->totalNumPages != numberOfPages) {
            appendEmptyBlock(fHandle);
        }
    }
    return RC_OK;
}


