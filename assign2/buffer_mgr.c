#include <stdlib.h>
#include "storage_mgr.h"
#include "buffer_mgr.h"


typedef struct {
    SM_PageHandle data;
    PageNumber pageNumber;
    bool dirty;
    int fixCount;
    int hitTime;
} Frame;

typedef struct {
    Frame *array;
    int writeCount;
    int readCount;
    int front;
    int rear;
    int count;
    int hitTime;
} Buffer;

RC FIFO(BM_BufferPool *const bm, Frame *page);

RC LRU(BM_BufferPool *const bm, Frame *page);

extern RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
                         const int numPages, ReplacementStrategy strategy,
                         void *stratData) {
    FILE *fp = fopen(pageFileName, "r");
    if (fp == NULL) {
        return RC_FILE_NOT_FOUND;
    }
    fclose(fp);

    bm->pageFile = (char *) pageFileName;
    bm->numPages = numPages;
    bm->strategy = strategy;
    Buffer *buffer = malloc(sizeof(Buffer));
    Frame *array = malloc(sizeof(Frame) * numPages);
    for (int i = 0; i < numPages; i++) {
        array[i].data = NULL;
        array[i].pageNumber = -1;
        array[i].dirty = 0;
        array[i].fixCount = 0;
    }
    buffer->array = array;
    buffer->readCount = 0;
    buffer->writeCount = 0;
    buffer->front = -1;
    buffer->rear = -1;
    buffer->hitTime = 0;
    buffer->count = 0;

    bm->mgmtData = buffer;
    return RC_OK;
}

extern RC shutdownBufferPool(BM_BufferPool *const bm) {
    forceFlushPool(bm);
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    free(array);
    buffer->array = NULL;
    free(buffer);
    bm->mgmtData = NULL;
    return RC_OK;
}

extern RC forceFlushPool(BM_BufferPool *const bm) {
    if (bm == NULL) {
        return RC_OK;
    }
    SM_FileHandle fh;
    openPageFile(bm->pageFile, &fh);

    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; i++) {
        if (array[i].fixCount == 0 && array[i].dirty) {
            Frame frame = array[i];
            if (writeBlock(frame.pageNumber, &fh, frame.data) != RC_OK) {
                printf("write to disk error happened for pageNumer: %d, data: %s", frame.pageNumber, frame.data);
            }
            array[i].dirty = FALSE;
            buffer->writeCount++;
        }
    }
    closePageFile(&fh);
    return RC_OK;
}

// Buffer Manager Interface Access Pages
extern RC markDirty(BM_BufferPool *const bm, BM_PageHandle *const page) {
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; i++) {
        if (array[i].pageNumber == page->pageNum) {
            array[i].dirty = TRUE;
            return RC_OK;
        }
    }
    return RC_OK;
}

extern RC unpinPage(BM_BufferPool *const bm, BM_PageHandle *const page) {
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; i++) {
        if (array[i].pageNumber == page->pageNum) {
            array[i].fixCount--;
            return RC_OK;
        }
    }
    return RC_OK;
}

extern RC forcePage(BM_BufferPool *const bm, BM_PageHandle *const page) {
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    SM_FileHandle fh;
    openPageFile(bm->pageFile, &fh);
    for (int i = 0; i < bm->numPages; i++) {
        if (array[i].pageNumber == page->pageNum) {
            writeBlock(array[i].pageNumber, &fh, array[i].data);
            array[i].dirty = FALSE;
            buffer->writeCount++;
        }
    }
    closePageFile(&fh);
    return RC_OK;
}

extern RC pinPage(BM_BufferPool *const bm, BM_PageHandle *const page,
                  const PageNumber pageNum) {
    if (pageNum < 0) {
        return RC_READ_NON_EXISTING_PAGE;
    }
    SM_FileHandle fh;
    openPageFile(bm->pageFile, &fh);
    page->pageNum = pageNum;
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    buffer->hitTime++;
    for (int i = 0; i < bm->numPages; i++) {
        if (array[i].pageNumber == pageNum) {
            array[i].fixCount++;
            array[i].hitTime = buffer->hitTime;
            return RC_OK;
        }
    }
    int rear = (buffer->rear + 1) % bm->numPages;
    bool isNotFull = buffer->count != bm->numPages;
    if (isNotFull) {
        buffer->rear = rear;
        array[rear].pageNumber = pageNum;
        array[rear].fixCount++;
        SM_PageHandle ph = malloc(sizeof(char) * PAGE_SIZE);
        ensureCapacity(pageNum + 1, &fh);
        readBlock(pageNum, &fh, ph);
        buffer->readCount++;
        array[rear].data = ph;
        page->data = array[rear].data;
        buffer->count++;
        array[rear].hitTime = buffer->hitTime;
        return RC_OK;
    }
    Frame *newPage = malloc(sizeof(Frame));
    newPage->pageNumber = pageNum;
    newPage->fixCount = 1;
    SM_PageHandle ph = malloc(sizeof(char) * PAGE_SIZE);
    ensureCapacity(pageNum + 1, &fh);
    readBlock(pageNum, &fh, ph);
    buffer->readCount++;
    newPage->data = ph;
    page->data = ph;
    newPage->dirty = FALSE;
    closePageFile(&fh);
    switch (bm->strategy) {
        case RS_FIFO:
            FIFO(bm, newPage);
            free(newPage);
            return RC_OK;
        case RS_LRU:
            LRU(bm, newPage);
            free(newPage);
            return RC_OK;
        case RS_CLOCK:
        case RS_LFU:
        case RS_LRU_K:
            return RC_REPLACE_STRATEGY_NOT_SUPPORTED;

    }
}

// Statistics Interface
extern PageNumber *getFrameContents(BM_BufferPool *const bm) {
    PageNumber *frameContents = malloc(sizeof(PageNumber) * bm->numPages);
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; i++) {
        frameContents[i] = array[i].pageNumber != NO_PAGE ? array[i].pageNumber : NO_PAGE;
    }
    return frameContents;
}

extern bool *getDirtyFlags(BM_BufferPool *const bm) {
    bool *dirtyFlags = malloc(sizeof(bool) * bm->numPages);
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; ++i) {
        dirtyFlags[i] = array[i].dirty;
    }
    return dirtyFlags;
}

extern int *getFixCounts(BM_BufferPool *const bm) {
    int *fixCounts = malloc(sizeof(int) * bm->numPages);
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    for (int i = 0; i < bm->numPages; ++i) {
        fixCounts[i] = array[i].fixCount != -1 ? array[i].fixCount : 0;
    }
    return fixCounts;
}

extern int getNumReadIO(BM_BufferPool *const bm) {
    Buffer *buffer = bm->mgmtData;
    return buffer->readCount;
}

extern int getNumWriteIO(BM_BufferPool *const bm) {
    Buffer *buffer = bm->mgmtData;
    return buffer->writeCount;
}

RC FIFO(BM_BufferPool *const bm, Frame *page) {
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;
    int front = (buffer->front + 1) % bm->numPages;
    buffer->front = front;
    while (array[front].fixCount > 0) {
        front = (buffer->front + 1) % bm->numPages;
        buffer->front = front;
        if (front < buffer->rear) {
            return RC_BUFFER_NOT_ENOUGH;
        }
    }
    if (array[front].dirty) {
        SM_FileHandle fh;
        openPageFile(bm->pageFile, &fh);
        writeBlock(array[front].pageNumber, &fh, array[front].data);
        array[front].dirty = FALSE;
        buffer->writeCount++;
        closePageFile(&fh);
    }
    array[front].pageNumber = page->pageNumber;
    array[front].dirty = FALSE;
    array[front].fixCount = 1;
    array[front].data = page->data;
    return RC_OK;

}


extern RC LRU(BM_BufferPool *const bm, Frame *page) {
    Buffer *buffer = bm->mgmtData;
    Frame *array = buffer->array;

    int i, leastHitIndex, leastHitTime;

    for (i = 0; i < bm->numPages; i++) {
        if (array[i].fixCount == 0) {
            leastHitIndex = i;
            leastHitTime = array[i].hitTime;
            break;
        }
    }

    for (i = leastHitIndex + 1; i < bm->numPages; i++) {
        if (array[i].hitTime < leastHitTime) {
            leastHitIndex = i;
            leastHitTime = array[i].hitTime;
        }
    }

    // If page in memory has been modified (dirtyBit = 1), then write page to disk
    if (array[leastHitIndex].dirty == 1) {
        SM_FileHandle fh;
        openPageFile(bm->pageFile, &fh);
        writeBlock(array[leastHitIndex].pageNumber, &fh, array[leastHitIndex].data);

        // Increase the writeCount which records the number of writes done by the buffer manager.

    }

    // Setting page frame's content to new page's content
    array[leastHitIndex].data = page->data;
    array[leastHitIndex].pageNumber = page->pageNumber;
    array[leastHitIndex].dirty = page->dirty;
    array[leastHitIndex].fixCount = page->fixCount;
    array[leastHitIndex].hitTime = buffer->hitTime;
    return RC_OK;
}




