# Assign 4

## Team member

- Mingming Lou
- Xiaobing Li
- Yue Li

## Files

The job consists of C files, header files and one makefile.

Run the command make to generate the executable file of the test.

## Description:

- initIndexManager(void *mgmtData)
- shutdownIndexManager()
  init and shutdown index manager


- createBtree(char *idxId, DataType keyType, int n)
  create btree and save to page

- openBtree(BTreeHandle **tree, char *idxId)
  open btree from page

- closeBtree(BTreeHandle *tree)
  close page file for btree

- deleteBtree(char *idxId)
  delete page file of the btree

- getNumNodes(BTreeHandle *tree, int *result)
  get total number of nodes for the btree

- getNumEntries(BTreeHandle *tree, int *result)
  get total number of entries for the btree

- getKeyType(BTreeHandle *tree, DataType *result)
  get data type of the key for the btree

- findKey(BTreeHandle *tree, Value *key, RID *result)
  search for key in all nodes, then each entries in the node

- insertKey(BTreeHandle *tree, Value *key, RID rid)
  insert key to the tree, find empty entry and insert node, the update tree

- deleteKey(BTreeHandle *tree, Value *key)
  delete key in the tree, then update tree

- openTreeScan(BTreeHandle *tree, BT_ScanHandle **handle)
  execute tree scan

- nextEntry(BT_ScanHandle *handle, RID *result)
  get next entry of the record

- closeTreeScan(BT_ScanHandle *handle) { close tree scan, reset number

- printTree(BTreeHandle *tree)
  print tree for debug