#include "btree_mgr.h"
#include "tables.h"
#include "storage_mgr.h"
#include "record_mgr.h"
#include <stdlib.h>
#include <string.h>


typedef struct BTREE {
    int *key;
    struct BTREE **next;
    RID *id;
} BTreeNode;

BTreeNode *root;
BTreeNode *scan;
int indexNum = 0;
int MaxEntriesInANode;
SM_FileHandle btree_fh;


RC createBtree(char *idxId, DataType keyType, int n) {
    root = (BTreeNode *) malloc(sizeof(BTreeNode));
    root->key = malloc(sizeof(int) * n);
    root->id = malloc(sizeof(int) * n);
    root->next = malloc(sizeof(BTreeNode) * (n + 1));
    for (int i = 0; i < n + 1; i++)
        root->next[i] = NULL;
    MaxEntriesInANode = n;
    createPageFile(idxId);
    return RC_OK;
}

RC openBtree(BTreeHandle **tree, char *idxId) {
    if (openPageFile(idxId, &btree_fh) == 0) {
        return RC_OK;
    } else {
        return RC_ERROR;
    }
}

RC closeBtree(BTreeHandle *tree) {
    if (closePageFile(&btree_fh) == 0) {
        free(root);
        return RC_OK;
    } else {
        return RC_ERROR;
    }
}

RC deleteBtree(char *idxId) {
    if (destroyPageFile(idxId) == 0) {
        return RC_OK;
    } else {
        return RC_ERROR;
    }
}

RC getNumNodes(BTreeHandle *tree, int *result) {
    BTreeNode *temp = (BTreeNode *) malloc(sizeof(BTreeNode));

    int num_nodes = 0;
    for (int i = 0; i < MaxEntriesInANode + 2; i++) {
        num_nodes++;
    }

    *result = num_nodes;


    return RC_OK;
}

RC getNumEntries(BTreeHandle *tree, int *result) {
    int num_entries = 0;
    BTreeNode *temp_node = (BTreeNode *) malloc(sizeof(BTreeNode));

    for (temp_node = root; temp_node != NULL; temp_node = temp_node->next[MaxEntriesInANode]) {
        for (int i = 0; i < MaxEntriesInANode; i++) {
            if (temp_node->key[i] != 0) {
                num_entries++;
            }
        }
    }
    *result = num_entries;
    free(temp_node);
    return RC_OK;
}

RC getKeyType(BTreeHandle *tree, DataType *result) {
    return RC_OK;
}

RC findKey(BTreeHandle *tree, Value *key, RID *result) {
    BTreeNode *temp_node = (BTreeNode *) malloc(sizeof(BTreeNode));
    int found = 0;
    for (temp_node = root; temp_node != NULL; temp_node = temp_node->next[MaxEntriesInANode]) {
        for (int i = 0; i < MaxEntriesInANode; i++) {
            if (temp_node->key[i] == key->v.intV) {
                (*result).page = temp_node->id[i].page;
                (*result).slot = temp_node->id[i].slot;
                found = 1;
                break;
            }
        }
        if (found == 1)
            break;
    }
    free(temp_node);

    if (found == 1)
        return RC_OK;
    else
        return RC_IM_KEY_NOT_FOUND;
}

RC insertKey(BTreeHandle *tree, Value *key, RID rid) {
    BTreeNode *temp = (BTreeNode *) malloc(sizeof(BTreeNode));

    // init new empty node
    BTreeNode *node = (BTreeNode *) malloc(sizeof(BTreeNode));
    node->key = malloc(sizeof(int) * MaxEntriesInANode);
    node->id = malloc(sizeof(int) * MaxEntriesInANode);
    node->next = malloc(sizeof(BTreeNode) * (MaxEntriesInANode + 1));
    for (int i = 0; i < MaxEntriesInANode; i++) {
        node->key[i] = 0;
    }

    // find empty entry and insert node
    int node_count = 0;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode]) {
        node_count = 0;
        for (int i = 0; i < MaxEntriesInANode; i++) {
            if (temp->key[i] == 0) {
                temp->id[i].page = rid.page;
                temp->id[i].slot = rid.slot;
                temp->key[i] = key->v.intV;
                temp->next[i] = NULL;
                node_count++;
                break;
            }
        }
        if ((node_count == 0) && (temp->next[MaxEntriesInANode] == NULL)) {
            node->next[MaxEntriesInANode] = NULL;
            temp->next[MaxEntriesInANode] = node;
        }
    }

    // calculate elements
    int total_element = 0;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode])
        for (int i = 0; i < MaxEntriesInANode; i++)
            if (temp->key[i] != 0)
                total_element++;

    // update data
    if (total_element == 6) {
        node->key[0] = root->next[MaxEntriesInANode]->key[0];
        node->key[1] = root->next[MaxEntriesInANode]->next[MaxEntriesInANode]->key[0];
        node->next[0] = root;
        node->next[1] = root->next[MaxEntriesInANode];
        node->next[2] = root->next[MaxEntriesInANode]->next[MaxEntriesInANode];

    }

    return RC_OK;
}

RC deleteKey(BTreeHandle *tree, Value *key) {
    BTreeNode *temp = (BTreeNode *) malloc(sizeof(BTreeNode));
    int found = 0, i;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode]) {
        for (i = 0; i < MaxEntriesInANode; i++) {
            if (temp->key[i] == key->v.intV) {
                temp->key[i] = 0;
                temp->id[i].page = 0;
                temp->id[i].slot = 0;
                found = 1;
                break;
            }
        }
        if (found == 1)
            break;
    }

    return RC_OK;
}

RC openTreeScan(BTreeHandle *tree, BT_ScanHandle **handle) {
    scan = (BTreeNode *) malloc(sizeof(BTreeNode));
    scan = root;
    indexNum = 0;

    BTreeNode *temp = (BTreeNode *) malloc(sizeof(BTreeNode));
    int totalEle = 0, i;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode])
        for (i = 0; i < MaxEntriesInANode; i++)
            if (temp->key[i] != 0)
                totalEle++;

    int key[totalEle];
    int elements[MaxEntriesInANode][totalEle];
    int count = 0;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode]) {
        for (i = 0; i < MaxEntriesInANode; i++) {
            key[count] = temp->key[i];
            elements[0][count] = temp->id[i].page;
            elements[1][count] = temp->id[i].slot;
            count++;
        }
    }

    int swap;
    int pg, st, c, d;
    for (c = 0; c < count - 1; c++) {
        for (d = 0; d < count - c - 1; d++) {
            if (key[d] > key[d + 1]) {
                swap = key[d];
                pg = elements[0][d];
                st = elements[1][d];

                key[d] = key[d + 1];
                elements[0][d] = elements[0][d + 1];
                elements[1][d] = elements[1][d + 1];

                key[d + 1] = swap;
                elements[0][d + 1] = pg;
                elements[1][d + 1] = st;
            }
        }
    }

    count = 0;
    for (temp = root; temp != NULL; temp = temp->next[MaxEntriesInANode]) {
        for (i = 0; i < MaxEntriesInANode; i++) {
            temp->key[i] = key[count];
            temp->id[i].page = elements[0][count];
            temp->id[i].slot = elements[1][count];
            count++;
        }
    }

    return RC_OK;
}

RC nextEntry(BT_ScanHandle *handle, RID *result) {
    if (scan->next[MaxEntriesInANode] != NULL) {
        if (MaxEntriesInANode == indexNum) {
            indexNum = 0;
            scan = scan->next[MaxEntriesInANode];
        }

        (*result).page = scan->id[indexNum].page;
        (*result).slot = scan->id[indexNum].slot;
        indexNum++;
    } else
        return RC_IM_NO_MORE_ENTRIES;

    return RC_OK;
}

RC closeTreeScan(BT_ScanHandle *handle) {
    indexNum = 0;
    return RC_OK;
}

RC initIndexManager(void *mgmtData) {
    return RC_OK;
}

RC shutdownIndexManager() {
    return RC_OK;
}

// debug and test functions
char *printTree(BTreeHandle *tree) {
    char *ch;
    BTreeNode *temp_node = (BTreeNode *) malloc(sizeof(BTreeNode));
    int found = 0;
    for (temp_node = root; temp_node != NULL; temp_node = temp_node->next[MaxEntriesInANode]) {
        printf("[");
        for (int i = 0; i < MaxEntriesInANode; i++) {
            if (temp_node->key[i] != 0) {
                printf("%d,", temp_node->key[i]);
            } else {
                break;
            }
        }
        printf("]");
    }
    free(temp_node);
    return ch;
}
